import psycopg2 
from psycopg2 import sql
import base as baseposgrest

class Classifications():

    def __init__(self,dt=None,id_st1=None,id_st2=None,duration=None,energy_mj=None,pmax=None,pred=None,note=None,max_crr=None,backaz=None,aparent_sound_speed=None):
        self.dt = dt
        self.id_st1 = id_st1
        self.id_st2 = id_st2
        self.duration = duration
        self.energy_mj = energy_mj 
        self.pmax = pmax
        self.pred = pred
        self.note = note
        self.max_crr = max_crr
        self.backaz = backaz
        self.aparent_sound_speed = aparent_sound_speed
    
    def addClassificationTable(self):
        sqlinsert = "insert into Classification(dt,id_st1,id_st2,duration,energy_mj,pmax,pred,note,max_crr,backaz,aparent_sound_speed) values('{}',{},{},{},{},{},{},'{}',{},{},{})".format(self.dt,self.id_st1,self.id_st2,self.duration,self.energy_mj,self.pmax,self.pred,self.note,self.max_crr,self.backaz,self.aparent_sound_speed)
        base = baseposgrest.Base()
        base.connect()
        base.load(sqlcmd=sqlinsert)
        base.disconnect()
        del base 
    def remove(self):
        sqlinsert = "delete from Classification where dt = '{}'".format(self.dt)
        base = baseposgrest.Base()
        base.connect()
        base.load(sqlcmd=sqlinsert)
        base.disconnect()
        del base
 
    def changeClassification(self,dt,id_st1,id_st2,duration,energy_mj,pmax,pred,note,max_crr,backaz,aparent_sound_speed):
        if (dt!=None):
            self.dt= dt
        if (id_st1!=None):
            self.id_st1= id_st1
        if(id_st2!=None):
            self.id_st2=id_st2
        if(duration!=None):
            self.location=duration
        if(energy_mj!=None):
            self.energy_mj=energy_mj  
        if(pmax!=None):
            self.pmax=pmax
        if(pred!=None):
            self.pred=pred
        if(note!=None):
            self.note=note
        if(max_crr!=None):
            self.max_crr=max_crr
        if(backaz!=None):
            self.backaz=backaz
        if(aparent_sound_speed!=None):
            self.aparent_sound_speed=aparent_sound_speed

        sqlinsert = "update Classification set id_st1={},id_st2={},duration={},energy_mj={},pmax={},pred={},note='{}',max_crr={},backaz={},aparent_sound_speed={} where dt = '{}'  ".format(self.id_st1,self.id_st2,self.duration,self.energy_mj,self.pmax,self.pred,self.note,self.max_crr,self.backaz,self.aparent_sound_speed,self.dt)
        base = baseposgrest.Base()
        base.connect()
        base.load(sqlcmd=sqlinsert)
        base.disconnect()
        del base
 
    def searchClassification(self,dt1=None,dt2=None):
        sqlinsert=""
        if(dt1 == 'ALL'):
            sqlinsert = "select * from Classification"
        
        if((dt1!=None)and(dt1!='ALL')and(dt2==None)):
            sqlinsert = "select * from Classification where dt = '{}' ".format(dt1)

        if((dt1!=None)and(dt1!='ALL')and(dt2!=None)):
            sqlinsert = "select * from Classification where dt >= '{}' AND dt< '{}' ".format(dt1,dt2)
        base = baseposgrest.Base()
        base.connect()
        rows = base.download(sqlcmd=sqlinsert)
        base.disconnect()
        del base
        return rows
 
import datetime
cla = Classifications()
da= cla.searchClassification('ALL')
print(da)
#cla  = Classifications(dt=datetime.datetime(2019,1,1,1,10),id_st1=1,id_st2=2,duration=2.2,energy_mj=2.5,pmax=0.2,pred=0.8,note="primera calssification",max_crr=1.2,backaz=0.47,aparent_sound_speed=320)
#cla.addClassificationTable()




