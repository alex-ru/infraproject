import obspy 
import numpy as np
import datetime
import json
import string
from scipy.signal import lfilter

#correlation
import cmath # para los complejos
from scipy import signal

def CorrSpecGram(xm,xs,nw,overlap,fs):
    win=signal.windows.hamming(nw)
    nx=len(xm)
    nxs=len(xs)

    #print("nx :"+str(nx))
    #print("nxs:"+str(nxs))
    if (nx!=nxs):
        print('two vectors mast have the same length ')
        return
    nwn= int((nx-overlap)/(nw-overlap))  # Number of windows
    #print('nwn ' + str(nwn))
    R = []#np.zeros((nwn))#,np.zeros(nwn)]
    A = []#np.zeros((nwn))#,np.zeros(nwn)]
    f = []
    for n in range(nwn):
        noff = (n)*(nw-overlap)
        xmw = xm[noff:noff+nw]
        xmw = signal.detrend(xmw)
        xmw = win*xmw
        xsw = xs[noff:noff+nw]
        xsw = signal.detrend(xsw)
        xsw = win*xsw
        y = np.correlate(xsw,xmw,"full") 
        y = y / np.linalg.norm(xsw)/np.linalg.norm(xmw)
        R.extend([y])
        #S = fft.fft1(R[n],fs)
        #if (len(f)<1):
            #f.extend([S[1]])
        #A.extend([S[2]])
    A=0
    tau= (np.arange(1,2*nw)-nw)/fs
    T = (np.arange(nwn)*(nw-overlap)+nw/2)/fs
    #T = (np.arange(0,nwn-1)*(nw-overlap)+nw/2)/fs
    
    #f=0
    return [tau,R,f,A,T]


def calculateCorrelogram(traza1,traza2,overlap=400,nw=2**9,nfilt=20,nlag=120):
    # funcion generica para 2 trazas 
    if(len(traza1.data)==0):
        print("No hay datos en la traza1 ")
        return 0
    if(len(traza2.data)==0):
        print("No hay datos en la traza2 ")
        return 0

    print("calculando correlograma ...")
    xm        =  traza1.data.copy()  
    xs        =  traza2.data.copy()  
    fs        =  traza1.head["sampling_rate"]  
    timeinit  =  traza1.head["starttime"]  
    timeend   =  traza1.head["endtime"]  

    if (len(xs)<len(xm)):
        xm = xm[0:len(xs)]
    
    if (len(xm)<len(xs)):
        xs = xs[0:len(xm)]

    for i in range(len(xm)):
        if(xm[i]==None):
            xm[i]=0
        if(xs[i]==None):
            xs[i]=0

    print(len(xs))    
    print(len(xm))
        
    [tau,R,f,A,T] = CorrSpecGram(xm,xs,nw,overlap,fs)
    R = np.transpose(R) 
    a=1; b= np.ones(nfilt)/nfilt
    S1= lfilter(b,a,R[nw-nlag:nw+nlag])
    ns = len(S1[0])
    S=[]
    for i in range(len(S1)):
        S.extend( [ S1[i][0:ns:nfilt] ] )

    Th=T[0:ns-1:nfilt]

    tau1 = tau[nw-nlag:nw+nlag]
    M    = S 
    fil = len(M)
    dataprocess = [fil,overlap,nw,nfilt,nlag]

    correl = Correlogram(head1=traza1.head,head2=traza2.head,datamatriz=M,tau=tau1,dataprocess=dataprocess)
    
    return correl

def convertToMatriz(vect,fil):
    size = len(vect)
    col = int(size/fil)
    #init = 0
    #end = col
    Matriz = []
    for i in range(0,len(vect),col):
        var = vect[i:i+col]
        Matriz.extend([var])
    return Matriz

def convertToVector(Matriz):
    vect = []
    vect1 = np.append(vect,Matriz)
    return vect1

def getvecttime(starttime,endtime,n):    

    dt = (datetime.timedelta.total_seconds(endtime - starttime)/n)
    xt = [starttime + datetime.timedelta(seconds=i*dt) for i in range(n)]
    return xt

def loadCorreMseed(pathfileload):
        
        buffer = obspy.read(pathfileload,format="MSEED")
        print("leido")
        datesvector = buffer.traces[0].data

        tau1 = buffer.traces[1].data
        dataprocess  = buffer.traces[2].data
        fil = dataprocess[0]

        head = {
        'network'       : buffer.traces[0].meta.network,
        'station'       : buffer.traces[0].meta.station,
        'channel'       : buffer.traces[0].meta.channel,
        'starttime'     :(buffer.traces[0].meta.starttime).datetime, 
        'endtime'       :(buffer.traces[1].meta.starttime).datetime,
        'sampling_rate' : buffer.traces[0].meta.sampling_rate,
        'location'      : buffer.traces[0].meta.location,
        #'calib': 0.044|
        }
        #print(head)
        print(dataprocess)
        matriz = convertToMatriz(datesvector,fil)
        correl = Correlogram(head1=head,head2=head,datamatriz=matriz,tau=tau1,dataprocess=dataprocess)
        return correl


def dataToShow(v,dt,time0,timef):
    vr=[]
    tr=[]
    t=0
    delta = int(len(v)/1000000)
    dt = datetime.timedelta.total_seconds(timef-time0)/len(v)
    for i in range(len(v)):
        if((i%delta)==0):
            t = time0 + datetime.timedelta(seconds=i*dt)
            vr.extend([v[i]])
            tr.extend([t])
    return [tr,vr]

class Traza():
    
    def __init__(self, data=[],head=None,datesview=[],vectimeview=[],stateview=0):
        self.data = data
        self.datesview = datesview
        self.vectimeview = vectimeview
        self.stateview = stateview
        self.head  = head
    
    def cleanTraza(self):
        self.data = []
        self.datesview = []
        self.vectimeview = []
        self.head = None
        self.stateview = 0
    

class Correlogram():
    
    def __init__(self,head1=None,head2=None,datamatriz=None,tau=None,dataprocess=None):

        self.head1 = head1
        self.head2 = head2
        self.datamatriz = datamatriz
        self.tau = tau
        self.dataprocess = dataprocess
    
    def getPathName(self):
        with open('config.json', 'r') as myfile:
            data=myfile.read()
        obj = json.loads(data)
        net1_='';code1_='';location1_='';channel1_='';net2_='';code2_='';location2_='';channel2_='';date_=''
        
        pathformat = obj['pathCorre']
        rutacorre = obj['rutaCorre']
        #pathformatcopy = obj['pathCorre']
        keys = [t[1] for t in string.Formatter().parse(pathformat) if t[1] is not None]
        
        for key in keys:
            pathformat = pathformat.replace(key,'')
            if('net1'in key):
                net1_=self.head1["network"]
            if('code1'in key):
                code1_=self.head1["station"]
            if('location1'in key):
                location1_=self.head1["location"]
            if('channel1'in key):
                channel1_=self.head1["channel"]
            if('net2'in key):
                net2_=self.head2["network"]
            if('code2'in key):
                code2_=self.head2["station"]
            if('location2'in key):
                location2_=self.head2["location"]
            if('channel2'in key):
                channel2_=self.head2["channel"]
            if('date'in key):
                dateformat  = key.replace('date','')                
                date_=self.head1["starttime"].strftime(dateformat)
        #print(dictkeys)
        #print(pathformat)
        pathname = pathformat.format(date_,net1_,code1_,location1_,channel1_,net2_,code2_,location2_,channel2_,date_)    
        
        # agregar ruta 
        return rutacorre + pathname +".mseed"


    def saveCorreMseed(self): #def saveCorreMseed(self,x=None,info=None,pathname="data"):
        print("guardando correlograma ...")
        datavector = convertToVector(self.datamatriz) # optener en forma de vector 
        #print("len vector mTRIZ")
        #print(len(self.datamatriz))
        tau1 = np.array(self.tau)
        #metadataprocess = np.arange(0) 
        metadataprocess = np.array(self.dataprocess)#np.append(metadataprocess,self.dataprocess)

        head1 = {
            'network': self.head1['network'],
            'station': self.head1['station'],
            'channel': self.head1['channel'],
            'starttime': self.head1['starttime'],
            #'endtime': datetime.datetime(2019,4,15,6,0,3),#self.head1['endtime'],
            'sampling_rate': self.head1['sampling_rate'],
            'location': self.head1['location'],
            #'calib': 0.044
            }
        head2 = {
            'network': self.head1['network'],
            'station': self.head1['station'],
            'channel': self.head1['channel'],
            'starttime': self.head1['endtime'], # usar este espacio para enviar el endtime se usa netamente para almasenar correlogramas
            #el endtime lo calcula la libreria obspy con el tamaño de los datos y la sample_rating 
            #'endtime': datetime.datetime(2019,4,15,6,0,3),#self.head1['endtime'],
            'sampling_rate': self.head1['sampling_rate'],
            'location': self.head1['location'],
            #'calib': 0.044
            }

        pathname =   self.getPathName()
        
        #print(self.head1)
        #print(self.head2)
        traza1 = obspy.core.trace.Trace(data=datavector,header=head1)        
        traza2 = obspy.core.trace.Trace(data=tau1,header=head2)
        traza3 = obspy.core.trace.Trace(data=metadataprocess,header=None)

        stream = obspy.core.stream.Stream([traza1,traza2,traza3])
        stream.write(pathname, format="MSEED")
        print("archivo " + pathname +" guardado")
        return 0
        
    
    def drawCorrelogram(self):
        print("dibujando correlograma ...")
        M = self.datamatriz
        tau = self.tau
        print("creando tiempo")
        t = getvecttime(self.head1["starttime"],self.head1["endtime"],len(M[0]))
    
        #print(len(M))
        #print(len(M[0]))
        #print(len(tau))
        #print(len(t))
        
        import matplotlib.pyplot as plt
        plt.pcolormesh(t,tau,M,cmap = plt.get_cmap('seismic'))
        #plt.pcolormesh(t,tau,M,cmap = plt.get_cmap('PiYG'))
        plt.show()

def prueba():
    path = 'D:/alejandro/trabajo/Volcano_Project_local/meneesed/Compartido2 con alejandro/signals/CM/2019/ACOLM/'
    file1 = "ACOLM.CM.02.HDF.2019.104.mseed"
    
    buffer = obspy.core.stream.read(path+file1,format="MSEED")
    trasa = buffer[0]
    
    print((trasa.stats.starttime).datetime)
    print((trasa.stats.endtime).datetime)

    data1 = trasa.data
    data2 = trasa.data

    head1 = {
        'network'       : trasa.stats.network,
        'station'       : trasa.stats.station,
        'channel'       : trasa.stats.channel,
        'starttime'     : (trasa.stats.starttime).datetime, 
        'endtime'       : (trasa.stats.endtime).datetime,
        'sampling_rate' : trasa.stats.sampling_rate,
        'location'      : trasa.stats.location,
        #'calib': 0.044|
    }
    
    print((trasa.stats.starttime).datetime)
    print((trasa.stats.endtime).datetime)

    [vecttime,dataview] = dataToShow(v=data1,dt=trasa.stats.delta,time0=(trasa.stats.starttime).datetime,timef=(trasa.stats.endtime).datetime)
    traza1 =Traza(data=data1,head=head1,datesview=dataview,vectimeview=vecttime,stateview=1)
    traza2 =Traza(data=data1,head=head1,datesview=dataview,vectimeview=vecttime,stateview=1)
    
    correl1 = calculateCorrelogram(traza1,traza2) 
    correl1.drawCorrelogram()
    correl1.saveCorreMseed()
    return 0
    

#corre1 = Correlogram(net1='CN',code1='MINO',location1='L',channel1='01',net2='CN',code2='COCO',location2='L',channel2='01',data=datos,tau=tau1,startime=inicio,endtime=fin,fs=fs1,dataprocess=metadata)
#corre1.saveCorreMseed()

#corre2 = loadCorreMseed('CN.MINO.L.01_CN.COCO.L.01_2019-01-01,00_00_00.mseed')
#corre2 = loadCorreMseed('2019-04-14.CM.ACOLM.02.HDF_CM.ACOLM.02.HDF_2019-04-14.mseed')
#corre2.drawCorrelogram()

#prueba()






