import psycopg2 
from psycopg2 import sql
import json

# servidor para base de prueba -- posible
# crear funcion executeSql() 

class Base():

    def load(self,sqlcmd):
        query =  sql.SQL(sqlcmd)  
        cur = self.con.cursor()
        cur.execute(query)
        self.con.commit()
        cur.close()
        #self.con.close()

    def download(self,sqlcmd):
        query =  sql.SQL(sqlcmd)
        cur = self.con.cursor()
        cur.execute(query)
        rows = cur.fetchall()
        #print(rows)
        cur.close()
        return rows

    def connect(self):
        with open('config.json', 'r') as myfile:
            data=myfile.read()
            obj = json.loads(data)
            base = obj['basedata']
            
            self.con = psycopg2.connect(
            host = base["host"],
            database=base["database"],
            user = base["user"],
            password = base["password"],
            port= base["port"])
            print("base conectada")
            print(self.con.get_dsn_parameters(),"\n") # datos de la coneccion

    def disconnect(self):
        self.con.close()
        print("base desconectada")
    
    def createTables(self):
        cur = self.con.cursor()
        query1 =  sql.SQL("Create table Classification( id serial primary key not null,dt timestamp,id_st1 int,Id_st2 int, duration Float, energy_mj Float, pmax Float,pred Float,note varchar(100),max_crr Float,backaz Float,aparent_sound_speed Float) ")
        query2 =  sql.SQL("Create table VPoint( id serial primary key not null,name varchar(20), code varchar(4), lat Float, lon Float, alt Float, volcano varchar(20) ) ")# 
        query3 =  sql.SQL("Create table Config( id serial primary key not null,mseedbase varchar(50), pathformat varchar(50), filenameformat varchar(50), corrfolder varchar(50), offset_fromUTC int) ")# 
        query4 =  sql.SQL("Create table Station( id serial primary key not null,stname varchar(20), code varchar(4), netw varchar(2), location varchar(20), unit varchar(10), lat Float, lon Float, alt Float, volcano varchar(20) ) ")# 
        query5 =  sql.SQL("Create table Station( id serial primary key not null,stname varchar(20), code varchar(4), netw varchar(2), location varchar(20), unit varchar(10), lat Float, lon Float, alt Float, volcano varchar(20) ) ")# 
        
        cur.execute(query1)
        self.con.commit()
        cur.execute(query2)
        self.con.commit()
        cur.execute(query3)
        self.con.commit()
        cur.execute(query4)
        self.con.commit()
        cur.execute(query5)
        self.con.commit()
        cur.close()
        self.con.close()

#base1 = Base()
#base1.connect()
#base1.disconnect()