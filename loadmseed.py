 
import obspy
import datetime
import time
import json
import os
from os import listdir
import numpy as np
import matplotlib.pyplot as plt
import string

def dataShow(v,dt,time0,timef):
    vr=[]
    tr=[]
    t=0
    dt = datetime.timedelta.total_seconds(timef-time0)/len(v)
    if(len(v)>1000000):
        delta = int(len(v)/1000000)
        #dt = datetime.timedelta.total_seconds(timef-time0)/len(v)
        for i in range(len(v)):
            if((i%delta)==0):
                t = time0 + datetime.timedelta(seconds=i*dt)
                vr.extend([v[i]])
                tr.extend([t])
    else:
        for i in range(len(v)):
            t = time0 + datetime.timedelta(seconds=i*dt)
            vr.extend([v[i]])
            tr.extend([t])

    return [tr,vr]

def loadMseedTraza(listfiles,timeinit,timeend):
    if(timeend<timeinit):
        print("la fecha final debe ser mayor que la fecha final")
        return 0
    dataf = np.arange(0)
    loaded = False
    i=0
    timeEnd_Old = timeinit
    

    while((i<len(listfiles))and(loaded==False)):#for i in range(listfiles):
        tiempo_i = timeinit.date()
        filenameread = listfiles[i]
        readed = False
        while(readed == False):
            #st = read(filenameread,format="MSEED")
            time_read = obspy.core.utcdatetime.UTCDateTime(tiempo_i)
            print(filenameread)
            buffer = obspy.core.stream.read(filenameread,format="MSEED",headonly=False,starttime=time_read)

            if(len(buffer)>0):
                trace = buffer[0]
                info = trace.stats
                y = trace.data
                fs = info.sampling_rate

                timeInitfile = (info.starttime).datetime
                timeEndfile =  (info.endtime).datetime
                
                if(timeInitfile>timeend): # en prueva puede tener problemas con las magnitudes de las trazas
                    loaded = True

                #print("start time "+ str(timeInitfile))
                #print("entime     "+ str(timeEndfile))
                #print(" ")

                if ((timeinit>=timeInitfile)and(timeinit<timeEndfile)):
                    
                    print("init file "+ str(timeInitfile))
                    print("end file "+str(timeEndfile))
                    print(filenameread)
                    print(" ")

                    y = trace.data
                    dt = datetime.timedelta.total_seconds(timeinit-timeInitfile)
                    ni = int(dt*fs) # numero de muestras para llegar hasta la sincronizacion
                    y = y[ni:]

                    if(timeend<timeEndfile):
                        dt = datetime.timedelta.total_seconds(timeend-timeinit)
                        nf = int(dt*fs)
                        y=y[0:nf]
                        loaded = True
                        timeEnd_Old = timeend
                    
                    if(timeend==timeEndfile):
                        loaded = True
                        timeEnd_Old = timeEndfile
                    if(timeend>timeEndfile):
                        timeEnd_Old = timeEndfile
                    
                    dataf = np.append(dataf,y)

                if ((timeInitfile>timeinit)and(timeInitfile<timeend)):
                    y = trace.data

                    print("init file "+ str(timeInitfile))
                    print("end file "+str(timeEndfile))
                    print(filenameread)
                    print(" ")

                    if(timeInitfile>timeEnd_Old):
                        dt = datetime.timedelta.total_seconds(timeInitfile-timeEnd_Old)
                        n = int(dt*fs)
                        dataf = np.append(dataf,[None]*n)
                        #dataf = np.append(dataf,np.zeros(n))
                    if(timeend<timeEndfile):
                        dt = datetime.timedelta.total_seconds(timeend-timeInitfile)
                        nf = int(dt*fs)
                        y=y[0:nf]
                        loaded = True
                        timeEnd_Old = timeend
                    if(timeend==timeEndfile):
                        loaded = True
                        timeEnd_Old = timeEndfile
                    if(timeend>timeEndfile):
                        timeEnd_Old = timeEndfile
                
                    dataf = np.append(dataf,y)
                if(timeEndfile <  ( datetime.datetime.combine(timeInitfile.date(),datetime.datetime.max.time()))):
                    tiempo_i = timeEndfile + datetime.timedelta(seconds=1/fs)
                
                if(timeEndfile >=  ( datetime.datetime.combine(timeInitfile.date(),datetime.datetime.max.time()))):
                    readed  = True

            if ((len(buffer)==0)or(loaded==True)):
                readed = True

        i=i+1
    if(loaded==False):
        print("no se pudo cargar datos en todo el rango de las fechas")
    
    if(len(dataf)==0):
        print("no se cargaron datos")
        return 0 # aqui termina la funcion load

    
    if(len(dataf)>0):
        [vecttime,dataview] = dataShow(dataf,1/fs,timeinit,timeEnd_Old)
        #vecttime=[];dataview =[]
        head1 = {
            'network'       : info.network,
            'station'       : info.station,
            'channel'       : info.channel,
            'starttime'     : timeinit,#(info.starttime).datetime, 
            'endtime'       : timeEnd_Old,#(info.endtime).datetime,
            'sampling_rate' : info.sampling_rate,
            'location'      : info.location,
            #'calib': 0.044
        }
        traza =Traza(data=dataf,head=head1,datesview=dataview,vectimeview=vecttime,stateview=1)
        #traza = Traza(dataf,dataview,vecttime,1,fs,timeinit,timeEnd_Old)

        return traza

class Traza():
    
    def __init__(self, data=[],head=None,datesview=[],vectimeview=[],stateview=0):
        self.data = data
        self.datesview = datesview
        self.vectimeview = vectimeview
        self.stateview = stateview
        self.head  = head
    
    def cleanTraza(self):
        self.data = []
        self.datesview = []
        self.vectimeview = []
        self.head = None
        self.stateview = 0
    
    def draw(self):        
        plt.figure()
        plt.plot(self.vectimeview,self.datesview,'k')
        plt.title(self.head["station"])
        plt.grid()
        plt.show()

def getRootFiles(foldername,net,code,timeinit,timeend):
    with open('config.json', 'r') as myfile:
        data=myfile.read()
    obj = json.loads(data)

    base = obj['base']
    pathfiles = obj['rootfiles']

    foldername_='';net_='';code_='';year_=''
    pathfiles = obj['rootfiles']
    keys = [t[1] for t in string.Formatter().parse(pathfiles) if t[1] is not None]
    for key in keys:
        pathfiles = pathfiles.replace(key,'')
        if('foldername' in key):
            foldername_ = foldername
        if('net'in key):
            net_= net
        if('code'in key):
            code_= code
        if('year'in key):
            yearformat  = key.replace('year','')                
            year1=timeinit.strftime(yearformat)
            year2=timeend.strftime(yearformat)
    
    if (timeinit.year == timeend.year):
        pathfile1 = pathfiles.format(foldername_,net_,year1,code_)  
        pathfiles = [base+pathfile1]
        return pathfiles

    if (timeinit.year != timeend.year):
        pathfile1 = pathfiles.format(foldername_,net_,year1,code_)    
        pathfile2 = pathfiles.format(foldername_,net_,year2,code_)    
        pathfiles = [base+pathfile1,base+pathfile2]
        return pathfiles

def getFilesInfra(pathroots,timeinit,timeend):
    
    fileschannel1 = []
    fileschannel2 = []
    
    init = timeinit.date()
    end  = timeend.date()

    for pathroot in pathroots:
        if os.path.isdir(pathroot): # se la ruta existe 
            filesExisted = listdir(pathroot) # lista de archivos dentro de esa ruta
            for fileE in filesExisted: # iterar la lista de archivos
                if ("mseed" in fileE): # si es mseed
                    origen = datetime.date(int(fileE.split('.')[4])-1,12,31)# origen 31 de dicimbre del año pasado
                    ndays = int(fileE.split('.')[5]) # numero del dia del archivo
                    timenow = origen + datetime.timedelta(days=ndays) # fecha del archivo
                    if((timenow>=init)and(timenow<=end)): # si el archivo esta entre las fechas de busqueda
                        if(fileE.split('.')[2]=='01'):   # si el archivo pertenece al channerl 01                     
                            fileschannel1.extend([pathroot+fileE])
                        if(fileE.split('.')[2]=='02'):   # si el archivo pertenece al channerl 01
                            fileschannel2.extend([pathroot+fileE])
        else:
            print("no existe la ruta " + pathroot)
    return[fileschannel1,fileschannel2]


def seachInfra(timeinit_,timeend_,code_):

    pathroots  = getRootFiles(foldername='signals',net='CM',code=code_,timeinit=timeinit_,timeend=timeend_)
    files = getFilesInfra(pathroots = pathroots,timeinit = timeinit_,timeend = timeend_)
    fileschannel1 = files[0]
    fileschannel2 = files[1]
    traza1 = loadMseedTraza(listfiles=fileschannel1,timeinit=timeinit_,timeend=timeend_)
    traza2 = loadMseedTraza(listfiles=fileschannel2,timeinit=timeinit_,timeend=timeend_)
    #traza1.draw()
    return[traza1,traza2]

# lo primero que se ejecuta

tiempo_inicio = datetime.datetime(2019,4,17,1,0)
tiempo_fin    = datetime.datetime(2019,4,17,23,59)

[traza1,traza2] = seachInfra(timeinit_=tiempo_inicio, timeend_=tiempo_fin,code_='ACRUM')
import correlogram_v1 as correlogram

correl1 = correlogram.calculateCorrelogram(traza1 = traza1,traza2=traza2) 
correl1.drawCorrelogram()

#filenameread1 =  "D:/alejandro/trabajo/Volcano_Project_local/meneesed/BasicPython/app/app12/signal/20200513 233421/20200513 233421CALA.mseed"
#filenameread2 =  "D:/alejandro/trabajo/Volcano_Project_local/meneesed/BasicPython/app/app12/signal/20200513 233421/20200513 233421CALZ.mseed" 

#tiempo_inicio = datetime.datetime(2020,5,13,23,34)
#tiempo_fin    = datetime.datetime(2020,5,13,23,36)
#traza1 = loadMseedTraza([filenameread1],tiempo_inicio,tiempo_fin)
#traza2 = loadMseedTraza([filenameread2],tiempo_inicio,tiempo_fin)
#traza1.draw()
#import correlogram_v1 as correlogram

#correl1 = correlogram.calculateCorrelogram(traza1 = traza1,traza2=traza2,overlap=1,nw=2**9,nfilt=1,nlag=120) 
#correl1.drawCorrelogram()
 

 

    
    


