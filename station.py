import psycopg2 
from psycopg2 import sql
import base as baseposgrest



#print( con.get_dsn_parameters(),"\n") # datos de la coneccion


class Stations():

    def __init__(self,stname=None,code=None,netw=None,location=None,unit=None,lat=None,lon=None,alt=None,volcano=None):
        self.stname = stname
        self.code = code
        self.netw = netw
        self.location = location
        self.unit = unit 
        self.lat = lat
        self.lon = lon
        self.alt = alt
        self.volcano = volcano
    

    #def addStation(self,stname,code,netw,location,unit,lat, lon, alt, volcano):
    def addStationtoTable(self):

        #query =  sql.SQL("insert into Station(stname,code,netw,location,unit,lat, lon, alt, volcano) values('stationname','ACIN','CO','Manizales','mVs',1.025, 25.03, 1200.5, 'volcano')") # 
        sqlinsert = "insert into Station(stname,code,netw,location,unit,lat, lon, alt, volcano) values('{}','{}','{}','{}','{}',{},{},{},'{}')".format(self.stname,self.code,self.netw,self.location,self.unit,self.lat,self.lon,self.alt,self.volcano)
        base = baseposgrest.Base()
        base.connect()
        base.load(sqlcmd=sqlinsert)
        base.disconnect()
        del base 


    def remove(self):

        sqlinsert = "delete from Station where stname = '{}'".format(self.stname)
        base = baseposgrest.Base()
        base.connect()
        base.load(sqlcmd=sqlinsert)
        base.disconnect()
        del base 

    
    def changeStation(self,stname=None,code=None,netw=None,location=None,unit=None,lat=None,lon=None,alt=None,volcano=None):


        if (stname!=None):
            self.stname= stname
        if (code!=None):
            self.code= code
        if(netw!=None):
            self.netw=netw
        if(location!=None):
            self.location=location
        if(unit!=None):
            self.unit=unit  
        if(lat!=None):
            self.lat=lat
        if(lon!=None):
            self.lon=lon
        if(alt!=None):
            self.alt=alt
        if(volcano!=None):
            self.volcano=volcano

        sqlinsert = "update Station set code='{}',netw='{}',location='{}',unit='{}',lat='{}',lon='{}', alt='{}', volcano='{}' where stname = '{}'  ".format(self.code,self.netw,self.location,self.unit,self.lat,self.lon,self.alt,self.volcano,self.stname)
        base = baseposgrest.Base()
        base.connect()
        base.load(sqlcmd=sqlinsert)
        base.disconnect()
        del base 
        #return

    
# crear remover element def
# adicionar class
    def searchStation(self,code=None):
        sqlinsert =""
        if(code == 'ALL'):
            sqlinsert = "select * from Station"
        else:
            sqlinsert = "select * from Station where code = '{}' ".format(code)
        base = baseposgrest.Base()
        base.connect()
        rows=base.download(sqlcmd=sqlinsert)
        base.disconnect()
        del base 
        print(rows)


#station1 = Stations('stationname2','ACIN','CO','popayan-Cauca','mVs',1.025, 25.04, 110.5, 'volcano')
#station1.changeStation('stationname','ACIN','CO','popayan-Cauca','mVs',1.025, 25.04, 110.5, 'volcano1')
#station1.addStationtoTable('stationname2','ACIN','CO','popayan-Cauca','mVs',1.025, 25.04, 110.5, 'volcano')
#station1.searchStation('ALL')
#station1.remove()
###createStationTable()
 



