import psycopg2 
from psycopg2 import sql
import base as baseposgrest

# clase para la base 
# config.json add info de base 
# servidor para base de prueba -- posible
# creacion de ramas en Git


class Configs():

    def __init__(self,mseedbase=None,pathformat=None,filenameformat=None,corrfolder=None,offset_fromUTC=None):
        self.mseedbase =  mseedbase
        self.pathformat =  pathformat
        self.filenameformat = filenameformat
        self.corrfolder = corrfolder
        self.offset_fromUTC = offset_fromUTC
    
    def addConfigTable(self):

        sqlinsert = "insert into Config(mseedbase,pathformat,filenameformat,corrfolder,offset_fromUTC) values('{}','{}','{}','{}',{})".format(self.mseedbase,self.pathformat,self.filenameformat,self.corrfolder,self.offset_fromUTC)
        base = baseposgrest.Base()
        base.connect()
        base.load(sqlcmd=sqlinsert)
        base.disconnect()
        del base 


    def remove(self,id1=0):

        sqlinsert = "delete from Config where id = {}".format(id1)
        base = baseposgrest.Base()
        base.connect()
        base.load(sqlcmd=sqlinsert)
        base.disconnect()
        del base 

    
    def changeConfig(self,mseedbase=None,pathformat=None,filenameformat=None,corrfolder=None,offset_fromUTC=None,id1=0):


        if (mseedbase!=None):
            self.mseedbase= mseedbase
        if (pathformat!=None):
            self.pathformat= pathformat
        if(filenameformat!=None):
            self.filenameformat=filenameformat
        if(corrfolder!=None):
            self.corrfolder=corrfolder
        if(offset_fromUTC!=None):
            self.offset_fromUTC=offset_fromUTC  
 
        sqlinsert = "update Config set mseedbase='{}',pathformat='{}',filenameformat='{}',corrfolder='{}',offset_fromUTC={} where id = {}  ".format(self.mseedbase,self.pathformat,self.filenameformat,self.corrfolder,self.offset_fromUTC,id1)
        base = baseposgrest.Base()
        base.connect()
        base.load(sqlcmd=sqlinsert)
        base.disconnect()
        del base 
 
    def searchConfig(self,id1=None):
 

        sqlinsert= ""
        if(id1 == 'ALL'):
            sqlinsert = "select * from Config"
        else:
            if(id1!=None):
                sqlinsert = "select * from Config where id = {} ".format(id1)
        base = baseposgrest.Base()
        base.connect()
        rows=base.download(sqlcmd=sqlinsert)
        base.disconnect()
        del base 
        print(rows)
 
 

#config1 = Configs(mseedbase='c:/alejandro',pathformat='path/otro',filenameformat='signals',corrfolder='folder1',offset_fromUTC=0)
#config2 = Configs(mseedbase='c:/alejandro2',pathformat='path2/otro',filenameformat='signals2',corrfolder='folder2',offset_fromUTC=1)
#config2.addConfigTable()
#config1.changeConfig(mseedbase='c:/alejandrocambioo',pathformat='path/1',filenameformat='signalscambio',corrfolder='foldercambio',offset_fromUTC=0,id1=1)
#config1.searchConfig('ALL')
#createConfigTable()
 



